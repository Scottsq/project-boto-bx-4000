const { prefix } = require('../config.json');

module.exports = {
	async gDocs(message) {
		await message.react('#⃣');
		await message.react('🇹');
		await message.react('🇪');
		await message.react('🇦');
		await message.react('🇲');
		await message.react('🇬');
		await message.react('🇩');
		await message.react('🇴');
		await message.react('🇨');
		await message.react('🇸');
	},
	getPrefix(message) {
		return message.channel.send(`Mon préfix est \`${prefix}\``);
	},
};
