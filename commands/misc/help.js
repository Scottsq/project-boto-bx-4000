const { prefix } = require('../../config.json');
const Discord = require('discord.js');
const fs = require('fs');
const rCheck = require('../../utils/rightcheck.js');
const reps = JSON.parse(fs.readFileSync('./utils/reps.json'));
const cmdUtils = require('../../utils/commandsUtil.js');

module.exports = {
	name: 'help',
	description: 'Liste toutes les commandes',
	alias: ['commandes', 'aide'],
	usage: '[command name]',
	serverOnly: false,
	args: false,
	cooldown: 3,
	execute(message, args) {
		const { commands } = message.client;
		const nomBot = message.client.user.username;
		const botAva = message.client.user.displayAvatarURL;

		//	si l'utilisateur utlise help sans arguments, on lui envoie un message en privé avec les commandes
		if(!args.length) {
			const allCommands = crRichEmbedCategories(message.author, message);
			//	on envoie le message
			return message.author.send(allCommands)
				.then(() => {
					//	si le message est envoyé en privé, on n'enclenche pas le timer, on envoie directement le message
					if (message.channel.type === 'dm') return;
					message.reply('Je t\'ai envoyé un message avec toutes les commandes. \nRéagissez avec un 👀 pour aussi recevoir le message (vous avez une minute).')
						.then(function(botMess) {
							//	On rajoute une réaction au message
							botMess.react('👀');
							//	On crée un filtre qui s'enclenche si l'emoji est :eyes:
							const filter = (reaction) => {
								return reaction.emoji.name === '👀';
							};

							//	on crée une ReactionCollector prenant en compte le filtre et on lui donne sa durée d'activité en ms.
							const collector = botMess.createReactionCollector(filter, { time: 60000 });
							//	a chaque fois que le collector reçoit une réaction renvoie le help
							collector.on('collect', (reaction) => {

								//	on fait en sorte de ne pas envoyer le message aux bots (le bot a réagit lui aussi au message...)
								if(!reaction.users.last().bot) {

									//	on récupère l'utilisateur ayant réagit sous forme de guildMember
									message.guild.fetchMember(reaction.users.last().id)
										.then(mbr => {
											//	On recrée un embed en fonction de l'utilisateur ayant réagi
											const allCommands2 = crRichEmbedCategories(mbr, message);

											//	On envoie le message au dernier utilisateur de la liste des gens qui ont réagi
											reaction.users.last().send(allCommands2);
										});
								}
							});

							//	à la fin, on indique aux utilisateurs qu'ils ne peuvent plus réagir...
							collector.on('end', () => {
								message.channel.send(`Fin de la minute, réutilisez \`${prefix}help\` pour le recevoir à nouveau`);
							});
						});
				})
				.catch(error => {
					console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
					message.reply('Il y a eu un problème quelque part, peut-être que vous n\'avez pas les messages privés d\'activé');
				});

		}
		//	si l'utilisateur veut des infos sur une commande en particulier ou une catégorie de commande
		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.alias && c.alias.includes(name));

		//	si elle n'existe pas.....
		if(!command) {
			if (reps.repertories.some((rep) => {
				return rep.name = name;
			})) {
				let cmdEmbedCat;
				if (message.channel.type === 'dm') {
					cmdEmbedCat = crRichEmbedCategory(message.author, message, name);
				}
				else {
					cmdEmbedCat = crRichEmbedCategory(message.member, message, name);
				}
				return message.author.send(cmdEmbedCat)
					.then(() => {
						//	si le message est envoyé en privé, on n'enclenche pas le timer, on envoie directement le message
						if (message.channel.type === 'dm') return;
						message.reply('Je t\'ai envoyé un message avec toutes les commandes de la catégorie. \nRéagissez avec un 👀 pour aussi recevoir le message (vous avez une minute).')
							.then(function(botMess) {
								//	On rajoute une réaction au message
								botMess.react('👀');
								//	On crée un filtre qui s'enclenche si l'emoji est :eyes:
								const filter = (reaction) => {
									return reaction.emoji.name === '👀';
								};

								//	on crée une ReactionCollector prenant en compte le filtre et on lui donne sa durée d'activité en ms.
								const collector = botMess.createReactionCollector(filter, { time: 60000 });
								//	a chaque fois que le collector reçoit une réaction renvoie le help
								collector.on('collect', (reaction) => {

									//	on fait en sorte de ne pas envoyer le message aux bots (le bot a réagit lui aussi au message...)
									if(!reaction.users.last().bot) {

										//	on récupère l'utilisateur ayant réagit sous forme de guildMember
										message.guild.fetchMember(reaction.users.last().id)
											.then(mbr => {
												//	On recrée un embed en fonction de l'utilisateur ayant réagi
												const allCommands2 = crRichEmbedCategories(mbr, message);

												//	On envoie le message au dernier utilisateur de la liste des gens qui ont réagi
												reaction.users.last().send(allCommands2);
											});
									}
								});

								//	à la fin, on indique aux utilisateurs qu'ils ne peuvent plus réagir...
								collector.on('end', () => {
									return;
								});
							});
					})
					.catch(error => {
						console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
						message.reply('Il y a eu un problème quelque part, peut-être que vous n\'avez pas les messages privés d\'activé');
					});
			}
			return message.reply('Je ne peux pas t\'aider, cette commande ou cette catégorie n\'existe pas');
		}

		//	on crée le embed a envoyé
		const cmdEmbed = new Discord.RichEmbed()
			.setColor('#10FFAA')
			.setTitle(`Aide pour la commande ${command.name}`)
			.setAuthor(`${nomBot}`, botAva)
			.setDescription(command.description)
			.setThumbnail(botAva)
			.addBlankField();

		//	on check si la commande possède des attributs particuliers
		if (command.alias) cmdEmbed.addField('Alias :', command.alias.join(', '), true);
		if (command.description) cmdEmbed.addField('Utilisation :', `${prefix}${command.name} ${command.usage}`, true);

		cmdEmbed.addField('Cooldown :', `${command.cooldown || 3} seconde(s)`, true)
			.setTimestamp()
			.setFooter(`${nomBot} by #TeamGDocs`, botAva);
		//	on check si la commande possède des sous commandes
		if (command.subCmd) {
			cmdEmbed.addField('Les sous-commandes :', '-------------------------');
			const subcmds = cmdUtils.getSubCommands(command.name);
			subcmds.array().forEach(subCmd => {
				cmdEmbed.addField(subCmd.name, subCmd.description);
			}
			);
		}
		//	on envoie le message
		message.channel.send(cmdEmbed);
	},
};

function crRichEmbedCategories(member, message) {

	const botAva = message.client.user.displayAvatarURL;
	const nomBot = message.client.user.username;
	//	on crée le embed a envoyé
	const allCommands = new Discord.RichEmbed()
		.setColor('#10FFAA')
		.setTitle('Help')
		.setAuthor(`${nomBot}`, botAva)
		.setDescription('Voici une liste de toutes les catégories de commandes :')
		.setThumbnail(botAva);

	// on vérifie si l'utilisateur a fait la requête en privé ou non
	reps.repertories.forEach(repz => {
		allCommands.addField(repz.name, repz.desc);
	});

	//	on rajoute des infos complémentaires et une petite signature :)
	allCommands.addBlankField()
		.addField('Plus d\'info sur une catégorie et ses commandes ?', `Utilise ${prefix}help [nom de la commande]`)
		.setTimestamp()
		.setFooter(`${nomBot} by #TeamGDocs`, botAva);

	//	on retourne l'embed
	return allCommands;
}

function crRichEmbedCategory(member, message, cat) {

	const botAva = message.client.user.displayAvatarURL;
	const nomBot = message.client.user.username;
	//	on crée le embed a envoyé
	const allCommands = new Discord.RichEmbed()
		.setColor('#10FFAA')
		.setTitle('Help')
		.setAuthor(`${nomBot}`, botAva)
		.setDescription(`Voici la liste des commandes de la catégorie ${cat} :`)
		.setThumbnail(botAva);

	// on vérifie si l'utilisateur a fait la requête en privé ou non
	if (message.channel.type === 'dm') {

		//	on ne peut pas vérifier le rôle de l'utilisateur, on lui envoie les commandes de base
		message.client.commands.array().forEach(cmd => {
			if (cmd.repertory === cat && ((cmd.status && (!cmd.ownerOnly) && (!cmd.adminOnly)) || rCheck.isOwner(member))) {
				allCommands.addField(cmd.name, cmd.description);
			}
		});
	}
	else {

		//	on vérifie pour chaque commande si l'utilisateur a le droit de l'utiliser et on les rajoutes
		message.client.commands.array().forEach(cmd => {
			if (cmd.repertory === cat && (cmd.status && (!cmd.ownerOnly) && (!cmd.adminOnly) || (rCheck.isAdmin(member) && cmd.status && (!cmd.ownerOnly)) || rCheck.isOwner(member))) {
				allCommands.addField(cmd.name, cmd.description);
			}
		});
	}

	//	on rajoute des infos complémentaires et une petite signature :)
	allCommands.addBlankField()
		.addField('Plus d\'info sur une commande ?', `Utilise ${prefix}help [nom de la commande]`)
		.setTimestamp()
		.setFooter(`${nomBot} by #TeamGDocs`, botAva);

	//	on retourne l'embed
	return allCommands;
}
