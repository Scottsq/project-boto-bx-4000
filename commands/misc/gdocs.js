module.exports = {
	name: 'gdocs',
	alias: ['googledocs', 'gdoc', 'googledoc'],
	description: 'Réagit de la manière la plus sublime qu\'il soit à un message',
	serverOnly: true,
	args: true,
	cooldown: 5,
	usage: '[id du message se trouvant sur le même salon]',
	execute(message, args) {
		message.channel.fetchMessage(args[0]).then(async function(msg) {
			await msg.react('#⃣');
			await msg.react('🇹');
			await msg.react('🇪');
			await msg.react('🇦');
			await msg.react('🇲');
			await msg.react('🇬');
			await msg.react('🇩');
			await msg.react('🇴');
			await msg.react('🇨');
			await msg.react('🇸');
		});
	},
};
