module.exports = {
	name: 'avatar',
	alias: ['ava', 'profilPic', 'pp'],
	description: 'Montre l\'avatar de l\'utilisateur mentionné!',
	serverOnly: true,
	args: true,
	usage: '[user]',
	execute(message, args) {
		const avatarList = message.mentions.users.map(user => {
			return `L'avatar de ${user.username} : <${user.displayAvatarURL}>`;
		});

		message.channel.send(avatarList);
	},
};
