module.exports = {
	name: 'joke',
	alias: ['blague'],
	description: "Raconte une blague. Lol.",
	serverOnly: false,
	args: false,
	cooldown: 0.1,
	execute(message, args) {
		blagues = require("../../datas/blagues.json");
    var test = "secret"
    var compteur = 0

    for (i in blagues) {
      if (i.indexOf(test) == -1) {
        compteur = compteur + 1;
      }
    }

    var x = Math.floor(Math.random()*compteur) //num de blague max + 1
    var secretomg = Math.floor(Math.random()*200) + 1
    var messageSecret = "Tu as débloqué une blague secrète!"

    if (secretomg == 69) {
      message.channel.send(messageSecret)
      for ( i = 0; i < blagues.secret1.length; i++) {
       message.member.send(blagues.secret1[i])
      }
    }
    else if (secretomg == 112) {
      message.channel.send(messageSecret)
      for ( i = 0; i < blagues.secret2.length; i++) {
        message.member.send(blagues.secret2[i])
      }
    }
    else {
      message.channel.send(blagues[x])
    }
	},
};
