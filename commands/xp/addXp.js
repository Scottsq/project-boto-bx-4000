const Discord = require('discord.js');
const User = require('../../datas/classesXp/user.js');
const rights = require('../../utils/rightcheck.js')

module.exports = {
	name: 'addxp',
	description: 'Donne de l\'xp à un user',
	serverOnly: true,
  usage: "[xp] [user]",
	cooldown: 1,
  args: true,
	execute(message, args) {
    if (rights.isOwner(message.author)){
      var member;
  		message.mentions.users.forEach(function(user){
  			member = user;
  		});
      var user = User.getUser(member.id);
      if (user == null) {
        user = new User(member.id, message.guild.id, 0, 0);
      }
      user.addXp(args[0]);
      return message.channel.send('`Ajout de ' + args[0] + 'xp à ' + member.username + ', xp actuel: ' + user.xp + 'xp`');
    }
    else return message.channel.send("`❌ Tu ne peux pas ajouter de l'xp à une personne`");
	},
};
