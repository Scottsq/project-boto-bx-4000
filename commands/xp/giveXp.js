const Discord = require('discord.js');
const User = require('../../datas/classesXp/user.js');
const rights = require('../../utils/rightcheck.js')

module.exports = {
	name: 'givexp',
	description: 'Transfère de l\'xp à un user',
	serverOnly: true,
  usage: "[xp] [user]",
	cooldown: 1,
  args: true,
	execute(message, args) {
    var member;
		message.mentions.users.forEach(function(user){
			member = user;
		});
    if (member == message.author) return message.channel.send("`❌ Tu ne peux pas te transférer ton xp !`");
    var user1 = User.getUser(message.author.id);
    if (user1 == null) {
      user = new User(message.author.id, message.guild.id, 0, 0);
    }
    var user2 = User.getUser(member.id);
    if (user2 == null) {
      user2 = new User(member.id, message.guild.id, 0, 0);
    }
    if (parseInt(args[0]) <= 0) return message.channel.send("`❌ Tu ne peux pas donner du vide !`")
    user1.addXp(parseInt(args[0]) * (-1));
    user2.addXp(parseInt(args[0]));
    return message.channel.send('`Transfert de ' + args[0] + 'xp à ' + member.username + ' de la part de ' + message.author.username + '\nxp actuel de ' + member.username + ': ' + user2.xp + ' xp`');
	},
};
