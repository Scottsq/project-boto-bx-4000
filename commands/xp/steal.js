const Discord = require('discord.js');
const Server = require('../../datas/classesXp/server.js');
const User = require('../../datas/classesXp/user.js');
const Vol = require('../../datas/classesXp/vol.js');
const {prefix} = require('../../config.json');

module.exports = {
  name: "steal",
  description: "Vol un montant aléatoire d'xp à un autre utilisateur",
  alias: ["vol"],
  args: true,
  usage: "[user]",
  cooldown: 0.1,
  serverOnly: true,
  execute(message, args){

    // On vérifie que le serveur dans lequel il se trouve accepte l'xp et le vol
    var server = Server.getServer(message.guild.id);
    if (server == null) return message.channel.send('`❌ Ce serveur n\'a pas encore été configuré pour l\'utilisation de l\'xp.' +
                                                    '\nPour configurer le serveur écrivez: ' + prefix + 'setup`');
    if (!server.acceptXp) return message.channel.send('`❌ Ce serveur n\'accepte pas l\'utilisation de l\'xp`');
    if (!server.acceptThief) return message.channel.send('`❌ Ce serveur n\' accepte pas l\'utilisation de la fonction vol`');

    // On récupère la personne mentionnée
    var member = message.mentions.members.first();
    if (member == null || member == undefined) return message.channel.send('`❌ Tu dois préciser une personne...`');

    // On essaye d'effacer le message
    try { message.delete(); }
    catch (err) {}

    // On récupère l'user victime et l'user voleur
    var victime = User.getUser(member.id);
    var voleur = User.getUser(message.member.id);
    if (victime == null || victime.xp < 20) {
      if (victime == null) User.baseUser(member.id, message.guild.id);
      return message.channel.send('`❌ Cet utilisateur n\'a pas assez d\'xp pour être volé !`');
    }
    if (voleur == null) voleur = User.baseUser(message.member.id, message.guild.id);

    // On vérifie pleins de choses :
    if (member.presence.status.toLowerCase() == "offline") return message.channel.send('`❌ Tu ne peux pas voler un utilisateur déconnecté`');
    if (!member.permissionsIn(message.channel).has("VIEW_CHANNEL")
        || !member.permissionsIn(message.channel).has("READ_MESSAGE_HISTORY")
        || !member.permissionsIn(message.channel).has("SEND_MESSAGES"))
        return message.channel.send('`❌ Tu ne peux pas voler un utilisateur qui n\'a pas accès à ce channel`');
    if (member === message.member) return message.channel.send('`❌ Tu ne peux pas te voler toi même`');
    if (member.user.bot) return message.channel.send("`❌ Tu ne peux pas voler un bot !`");

    // On commence par calculer le montant d'xp volé
    var max = 25 * (((1+voleur.getThiefLevel())/10)+1) * voleur.getThiefLevel() + (25*(voleur.getThiefLevel()-1))
    var amount = Math.floor((Math.random() * max) + 1);
    // On calcul le temps de réaction en seconde en fonction du montant d'xp volé et du niveau de voleur du voleur
    var tps = parseInt(Date.now()) + Math.floor(((amount / ((voleur.getThiefLevel()+1/10) + 1)) * 5) * 1000);
    var vol = new Vol(message.member, member, amount, tps);

    // On envoie un message pour signaler le vol
    message.channel.send("<@" + member.user.id + ">, tu t'es fait voler **" + amount + "xp** ! Ecris `" + prefix + "vu` avant qu'il ne soit trop tard et que tu ne perdes ton xp... (" + Math.floor((tps - Date.now()) / 1000) + " secondes)");

    // On déclenche le timer
    setTimeout(function(){
      // Si la victime n'a pas réagit elle perd son xp et le voleur le gagne.
      if (!vol.vu){
        voleur.addXp(amount);
        voleur.addThiefXp(Math.floor((Math.random() * max-5) + 5))
        victime.addXp(amount * (-1));
      }
      Vol.clearVols(message.member);
    }, tps - Date.now());
  },
}
