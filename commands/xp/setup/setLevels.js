const Discord = require('discord.js');
const Server = require('../../../datas/classesXp/server.js');

module.exports = {
	name: 'setlevels',
	description: 'Définit les paliers d\'xp à atteindre pour monter en niveaux | Définit les paliers d\'xp de voleur à atteindre pour monter en niveaux',
	serverOnly: true,
  args: true,
  usage: '[xp 1] [xp 2] ... [xp n]',
	execute(message, args, attrib) {
			// On trie la liste donnée par les args au cas où ça ne serait pas dans l'ordre
			var list = new Array();
			args.forEach(function(val){
				list.push(val);
			})
			list.sort(function(a, b){return a-b});

			// On ne veut pas 2 paliers identiques
			var text = list[0].toString() + ', ';
			var paliers = [list[0]];
			for (var i = 1; i < list.length; i++) {
		    if (list[i-1] !== list[i]) {
		      paliers.push(list[i]);
					text += list[i].toString() + ', ';
		    }
		  }

			// On récup le serveur (ou on le crée)
      var server = Server.getServer(message.guild.id);
      if (server == null) server = Server.baseServer(message.guild.id);

			// On attribue les paliers en fonction de l'attribut
			if (attrib == "xp") server.setLvlsXp(paliers);
			else server.setLvlsThief(paliers);
      return message.channel.send('`✔ Nouveaux paliers ' + ((attrib == "xp") ? 'd\'xp' : 'd\'xp de voleur') + ': [' + text.substring(0, text.length - 2) + ']`');
	},
};
