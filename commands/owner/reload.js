const status = require('../../utils/status.json');
const fs = require('fs');
const reps = JSON.parse(fs.readFileSync('./utils/reps.json'));

module.exports = {
	name: 'reload',
	description: 'Recharge une commande',
	args: true,
	execute(message, args) {
		console.log('\n\n==========================================================================');
		console.log('\n\n     ||====================================||');
		console.log('     ||   Starting to reload commands...   ||');
		console.log('     ||====================================||\n');
		switch (args[0]) {
		case 'all': reloadAll(message); break;
		default : {
			if (message.client.commands.has(args[0])) { reloadCmd(message, message.client.commands.get(args[0]).repertory, args[0]); }
			else if (fs.readdirSync('./commands').filter(file => !file.includes('.')).includes(args[0])) { reloadRep(message, args[0]); }
			else { return message.channel.send('Ptdr tékon ce que t\'as mis n\'est pas un argument valide!'); }
		}
		}
		console.log('\n\n==========================================================================\n\n');
	},
};
function reloadAll(message) {
	const repertories = fs.readdirSync('./commands').filter(file => !file.includes('.'));
	repertories.forEach(rep => {
		reloadRep(message, rep);
	});
}

function reloadRep(message, rep) {
	try{
		const commandFiles = fs.readdirSync(`./commands/${rep}`).filter(file => file.endsWith('.js'));
		console.log(`\n+----+ Reloading repertory : ${rep}\t\t=> ${commandFiles.length} commands`);
		//	On charge les commandes
		for (const cmd of commandFiles) {
			reloadCmd(message, rep, cmd.split('.')[0]);
		}
		let repExist = false;
		reps.repertories.forEach(repz => { if (repz.name == rep) { repExist = true;	}});
		if (!repExist) {
			reps.repertories.push({ 'name':`${rep}`, 'desc':`Toutes les fonctions de ${rep}` });
		}
	} catch(err) {
		console.error(err);
		console.log();
	}
	fs.writeFileSync('./utils/reps.json', JSON.stringify(reps, null, 2));
	fs.writeFileSync('./utils/status.json', JSON.stringify(status, null, 2));
}

function reloadCmd(message, rep, cmd) {
	try{
		console.log(`     |\n     |----- Reloading command : ${cmd}.js...`);
		delete require.cache[require.resolve(`../../commands/${rep}/${cmd}.js`)];
		message.client.commands.delete(cmd);
		const command = require(`../../commands/${rep}/${cmd}`);
		command.repertory = rep;

		//	si la commande n'est pas encore dans status.json, on les rajoutes
		if(!Object.keys(status).some(key => key === command.name)) {
			status[command.name] = true;
		}
		command.status = status[command.name];
		message.client.commands.set(command.name, command);
		console.log('     |     => Successfully reloaded.');
	}catch(err) {
		console.error(err);
		console.log();
	}
}
