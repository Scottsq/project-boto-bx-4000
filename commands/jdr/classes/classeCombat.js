const util = require('../utilitaires/utilitaires.js');
const Discord = require('discord.js');
const fs = require('fs');

module.exports = class classeCombat {
	constructor(postEdit) {
		if(postEdit == 0) {
			this.appel = false;
			this.tour = 0;
			this.combat = false;
			this.present = new Array();
			this.ordre = new Array();
		} else {
			this.appel = postEdit.appel;
			this.tour = postEdit.tour;
			this.combat = postEdit.combat;
			this.present = postEdit.present;
			this.ordre = postEdit.ordre;
		}
	}

	enCombat() { return this.combat; }
	setCombat(bool, msg) {
		try{
			this.combat = bool;
			msg.channel.send('<@' + util.getJoueur((this.ordre[this.tour].split(':'))[0]) + '> alias ' + (this.ordre[this.tour].split(':'))[0] + ' c\'est a ton tour! Envoie b!done pour finir ton tour.');
		}catch(err) {
			msg.channel.send('Il n\'y a aucun combattant!!!');
			this.fin(msg);
		}
	}

	appelEnCours() { return this.appel; }
	setAppel(bool) { this.appel = bool; }

	getTour() { return this.tour; }
	tourSuivant(msg) {
		if (this.combat) {
			if (util.getJoueur((this.ordre[this.tour].split(':'))[0]) == msg.author.id && this.tour < this.ordre.length - 1) {
				msg.channel.send('Fin du tour de ' + (this.ordre[this.tour].split(':'))[0] + '!');
				this.tour++;
				msg.channel.send('<@' + util.getJoueur((this.ordre[this.tour].split(':'))[0]) + '> alias ' + (this.ordre[this.tour].split(':'))[0] + ' c\'est a ton tour! Envoie b!done pour finir ton tour.');
			} else if (util.getJoueur((this.ordre[this.tour].split(':'))[0]) == msg.author.id && this.tour == this.ordre.length - 1) {
				msg.channel.send('Fin du tour de ' + (this.ordre[this.tour].split(':'))[0] + '!');
				this.tour = 0;
				this.combat = false;
				msg.channel.send('***FIN DU ROUND***');
			}
			else {
				msg.channel.send('<@' + msg.author.id + '> c\'est pas ton tour couillon!');
			}
		} else {
			msg.channel.send('Pas de combat en cours.');
		}
	}

	showOrdre(msg) {
		let quand = '';
		let j = 0;
		let newTour = true;
		let tourNom = '';
		const embed = new Discord.RichEmbed()
			.setTitle('Ordre de passage:')
			.setColor(2671393)
			.setThumbnail('https://i.imgur.com/gsSKMuS.gif');

		for (let i = 0; i <= 10; i++) {
			newTour = true;
			quand = '';
			while (j < this.ordre.length && this.ordre[j].split(':')[1] == i) {
				if (newTour) {
					tourNom = 'Tour ' + i + '\n';
					newTour = false;
				}
				quand += (this.ordre[j].split(':'))[0] + '/';
				j++;
			}
			if (tourNom.length != 0) {
				embed.addField(tourNom, quand.substring(0, quand.length - 1));
				tourNom = '';
			}
		}
		msg.channel.send({ embed });
	}

	nouveauRound() { this.tour = 0; }

	getCombattants() { return this.present; }
	ajouterCombattant(nom) { this.present.push(nom); }

	getOrdre() { return this.ordre; }
	initOrdre() {
		const cbt = this;
		this.getCombattants().forEach(function(nom) {
			util.jdrJetsCmd(3, 10).forEach(function(jets) {
				cbt.ordre.push(nom + ':' + jets);
			});
		});
	}

	trierOrdre() {
		this.ordre.sort(function(a, b) {
			return parseInt((a.split(':'))[1]) - parseInt((b.split(':'))[1]);
		});
	}

	fin(msg) {
		this.appel = false;
		this.tour = 0;
		this.combat = false;
		this.present = new Array();
		this.ordre = new Array();
		msg.channel.send('Fin du combat.');
	}

	static fromJSON() {
		const combat = JSON.parse(fs.readFileSync('./temp/combat.json'));
		return new this(combat);
	}

	toJSON() {
		return {
			appel: this.appel,
			tour: this.tour,
			combat: this.combat,
			present: this.present,
			ordre: this.ordre };
	}

	stockerJSON() {
		fs.writeFile('./temp/combat.json', JSON.stringify(this), function(err) {
			if (err) {
				console.log(err);
			}
		});
	}
};
