const combat = require('../classes/classeCombat.js');
const util = require('../utilitaires/utilitaires.js');

module.exports = {
	name: 'rdy',
	description: 'Indique qu\'un joueur va participer au combat',
	alias: ['ready', 'unready'],
	tmp: true,
	execute(msg, args) {
		try{
			const monCombat = combat.fromJSON();
			const nom = util.getPerso(msg.author.id);
			if (monCombat.appelEnCours()) {
				if (!(monCombat.getCombattants().includes(nom)) && nom.length > 2) {
					monCombat.ajouterCombattant(nom);
				}
				else if (msg.author.id == '192933855110365184' && args[0].length != 0) {
					monCombat.ajouterCombattant(args[0]);
				}
				monCombat.stockerJSON();
			} else {
				msg.channel.send('Pas d\'appel en cours!');
			}
			monCombat.stockerJSON();
		}
		catch (err) {
			console.error(err);
		}
	},
};