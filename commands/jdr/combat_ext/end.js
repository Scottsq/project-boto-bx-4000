const combat = require('../classes/classeCombat.js');
require('discord.js');

module.exports = {
	name: 'end',
	description: 'Mets fin au combat',
	tmp: true,
	execute(msg, args) {
		try{
			if (msg.author.id == '192933855110365184') {
				const monCombat = combat.fromJSON();
				monCombat.fin(msg);
				delete require.cache[require.resolve('./done.js')];
				delete require.cache[require.resolve('./end.js')];
				msg.client.commands.delete('done');
				msg.client.commands.delete('end');
				monCombat.stockerJSON();
			}
		}
		catch (err) {
			console.error(err);
		}
	},
};