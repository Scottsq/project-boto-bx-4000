const {listUser} = require('./listUser.json');
const fs = require('fs');
const Server = require('./server.js');

module.exports = class User {
    // Constructeurs ----------------------------------------------------------
    constructor(userId, serverId, xp, thiefXp){
        this.id = userId;         // long
        this.serverId = serverId; // long
        this.xp = xp;             // int
        this.thiefXp = thiefXp;   // int
        this.saveUser();
    }
    static baseUser(userId, serverId){ return new User(userId, serverId, 0, 0); }

    // Gets / Sets ------------------------------------------------------------
    static getUser(id){
        var res = null;
        listUser.forEach(function(user) {
          if (user.id == id) res = user;
        });
        if (res != null) return new User(res.id, res.serverId, res.xp, res.thiefXp);
        else return res;
        //return res;
    }
    getXpLevel(){
      var serv = Server.getServer(this.serverId);
      if (serv == null){
        serv = Server.baseServer(this.serverId);
      }
      return serv.getLvl(this.xp)
    }
    getThiefLevel(){
      var serv = Server.getServer(this.serverId);
      if (serv == null){
        serv = Server.baseServer(this.serverId);
      }
      return serv.getLvl(this.thiefXp);
    }

    // Méthodes ---------------------------------------------------------------
    addXp(xp){
      this.xp += parseInt(xp);
      if (this.xp < 0) this.xp = 0;
      this.saveUser();
    }
    addThiefXp(xp){
      this.thiefXp += parseInt(xp);
      if (this.thiefXp < 0) this.thiefXp = 0;
      this.saveUser();
    }
    saveUser(){
      // Si l'user est déjà dans la liste
      for (var i=0; i<listUser.length; i++){
        if (listUser[i].id == this.id)
          listUser.splice(i, 1);
      }
      listUser.push(this);
      var toJson = '{ "listUser": ' + JSON.stringify(listUser, null, 2) + '\n}';
      fs.writeFile(__dirname + '/listUser.json', toJson,
                   function(err){ if (err) console.log(err); });
    }

};
