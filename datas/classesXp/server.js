const {listServer} = require('./listServer.json');
const fs = require('fs');

module.exports = class Server {
  // Constructeurs -------------------------------------------------------------
  constructor(serverId, acceptXp, acceptThief){
    this.id = serverId;             // long
    this.acceptXp = acceptXp;       // bool
    this.acceptThief = acceptThief; // bool
    this.saveServer();
  }
  static baseServer(serverId){
    return new Server(serverId, true, true);
  }

  // Gets / Sets --------------------------------------------------------------
  setAcceptXp(accept){ this.acceptXp = accept; this.saveServer(); }
  setAcceptThief(accept){ this.acceptThief = accept; this.saveServer(); }
  getLvl(xp){
    var lvl = 1;
    var amount = 0;
    while (xp >= amount){
      amount = 25 * lvl * lvl;
      lvl++;
    }
    if (lvl == 1) lvl = 2;
    return lvl-1;
  }
  static getServer(id){
    var res = null;
    listServer.forEach(function(server) {
      if (server.id == id) res = server;
    });
    if (res != null) return new Server(res.id, res.acceptXp, res.acceptThief);
    else return res;
  }

  // Methodes -----------------------------------------------------------------
  saveServer(){
    // Si le server est déjà dans la liste
    for (var i=0; i<listServer.length; i++){
      if (listServer[i].id == this.id)
        listServer.splice(i, 1);
    }
    listServer.push(this);
    var toJson = '{ "listServer": ' + JSON.stringify(listServer, null, 2) + ' }';
    fs.writeFile(__dirname + '/listServer.json', toJson,
                 function(err){ if (err) console.log(err); });
  }
  static nextLvl(xp){
    var i = 1;
    var lvl = 0;
    while (xp > lvl ){
      lvl = 25 * i * i;
      i++;
    }
    if (xp == 0) return 25;
    return lvl;
  }
  static previousLvl(xp){
    var i = 1;
    var lvl = 0;
    while (xp > lvl ){
      lvl = 25 * i * i;
      i++;
    }
    if (i > 1) return (25 * (i-2) * (i-2));
    else return 0;
  }
}
